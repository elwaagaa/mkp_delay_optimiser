#TEST SCRIPT TO CHECK THE RELEVANT BPM VARIABLES FOR THE SPS

import numpy as np
import pandas as pd
import pickle
from pathlib import Path
import sys
import logging
from pyjapc import PyJapc
import seaborn as sns
import matplotlib.pyplot as plt
from cern_general_devices.bpm import BPMLHC  #BPM device, normally for LHC

# --------- PLAY WITH ACTUAL DATA OR JUST SIMULATED DATA ---------
real_bpm_data = True
fill_own_bpm_values = False #if want to play with empty dataframe and fill in indices 
load_old_bpm_values = False #if no cycle running, load old BPM values 
# ---------------------------------------------------------------

#Specify if single index, if not how large wavewidth is
single_index = True
bunch_index_width = 1

#If BPM data doesn't already exist, uncomment the following section to extract BPM data
#"""
#Initiate japc and bpm device 
japc = PyJapc("SPS.USER.LHC1", noSet=True)
bpm = BPMLHC(japc)  #bpm of LHC type

if real_bpm_data:
    n_turns = bpm.n_turns
    print("\nNumber of BPM turns: {}\n".format(n_turns))
    bpm_pos_now = bpm.getParameter()


    x = bpm_pos_now[0]['hor'][1] 
    print("BPM position now: {}".format(x)) #horizontal position of 2nd BPM (SPS.BPMB.51503)
    print(x)

    #Turn the matrix into a dataframe
    df_org = pd.DataFrame(x)

    #save the dataframe to csv, unless already done
    #df_org.to_csv('bpm_data/BPMB_51303_hor_bpm_data.csv', index=False, header=False)

    #UNCOMMENT BELOW IF JUST WANT TO DISPLAY THE UNPROCESSED BPM DATA 
    #ax = sns.heatmap(df_org) 
    #plt.show()
    #quit()

# ------------------------ TEST FUNCTION TO DIRECTLY EXTRACT USEFUL BUNCHES ----------
def extract_useful_bunches(bpm=bpm, bunches_per_batch = 1):
    bpm_pos_now = bpm.getParameter()
    bpm_pos_injected = []
    bpm_pos_circulating = []
    return_data = False
        # -------- extract the injected and circulating beam to plot injection oscillations ----- 
    for i in range(3):   #iterate over all BPMs 
        print("Starting iteration {}".format(i+1))
        df = pd.DataFrame(
                bpm_pos_now[0]['hor'][i]  #horizontal position of 2nd BPM (SPS.BPMB.51503)
            )
        df = df.loc[
            :, (df != 0).any(axis=0)
            ]  # remove all zero-valued columns, where there is no injected nor circulating beam
        if df.empty:
                logging.warning("\nEmpty BPM positions, waiting...\n")
        else:
            if (
                len(df.columns) == 1
                or df.astype(bool).sum(axis=0).iloc[0] != 300
                ):
                logging.warning(
                    "\nInjected or circulating beam lost...\n"
                    )
            else:
                return_data = True
                waveslice = np.array(
                    df.iloc[0, :]
                    )  # projection of the waveforms, first row
                    
                # Find the indices of correct rows corresponding to the injected and circulating beam
                if bunch_index_width == 1:
                    x1 = np.array(df.iloc[:, 0])
                    x2 = np.array(df.iloc[:, 1])
                else:
                    indices = waveslice.nonzero()[
                        0
                    ]  # find indices of non-zero elements
                    ind_start = indices[0]
                    ind1 = (
                        ind_start + bunch_index_width
                    )  # if waveform index width is know, pick the last one
                    ind2 = indices[np.where(indices > ind1)][
                        0
                    ]  # second index simply the next value of "indices", larger than ind1
                    # bpm position vectors for injected and circulating
                    x1 = np.array(df.iloc[:, ind1])
                    x2 = np.array(df.iloc[:, ind2])
                    
                #Append the injected and circulating BPM positions for each BPM 
                bpm_pos_injected.append(x1)
                bpm_pos_circulating.append(x2)

                print("\nIteration {} injected data: {}".format(i+1, bpm_pos_injected[i]))
    
    if return_data:
        return bpm_pos_injected, bpm_pos_circulating

if real_bpm_data:
    #Try to call the above function to extract the BPM data
    data = extract_useful_bunches()
    #print("BPM position with TESTFUNCTION: {}".format(data))
    quit()

# ------------------------ TEST SOME OPERATIONS ON EMPTY DATAFRAME --------------------
#"""
if fill_own_bpm_values:
    df_empty = pd.DataFrame({'A': []})
    
    print("\n Empty dataframe: {}\n".format(df_empty))
    
    df_empty_check = df_empty.loc[(df_empty != 0).any(axis=1), :]  #remove all zero-valued columns, i.e. before injction 
    df_empty_check = df_empty_check.loc[:, (df_empty_check != 0).any(axis=0)] 
    print("\n Empty dataframe 2: {}\n".format(df_empty_check))
    
    print("Empty dataframe length: {}".format(len(df_empty.columns)))
    print("Dataframe empty: {}\n".format(df_empty.empty))

    #If only zero-valued, add simulated values
    df_org = pd.read_csv('bpm_data/single_index_real_BPMB_51303_hor_bpm_data.csv', header=None)
    #df_org.loc[100:, 55] = 10      #Add only circulating beam, not injected 
    #df_org.loc[:, 45] = 20      #Add only circulating beam, not injected 

    df = df_org.loc[:, (df_org != 0).any(axis=0)]  #remove all zero-valued columns,
    #df = df.loc[(df != 0).any(axis=1), :]  #remove all zero-valued rows, i.e. before injction 

    ax = sns.heatmap(df_org) 
    ax.set(xlabel="Bunch index number", ylabel="#turns")
    ax.title.set_text("SPS BPM number {}".format(1))
    plt.show()


#----------- IF NO CYCLE, READ ALREADY SAVED DATA ----------------------
#Read already saved BPM data into dataframe 
if load_old_bpm_values:
    df_org = pd.read_csv('bpm_data/single_index_real_BPMB_51303_hor_bpm_data.csv', header=None)


# -------------- OLD TESTS, NOW CONTAINED IN THE FUNCTION "extract_useful_bunches()" -----------

"""
non_zero_number = df.astype(bool).sum(axis=0).iloc[0]
print("\nNumber of non-zero elements in first column: {}\n".format(non_zero_number))

ax = sns.heatmap(df) 
plt.show()

#df = pd.DataFrame({'A': []})

if len(df.columns) == 1 or len(df.columns) == 0 or df.astype(bool).sum(axis=0).iloc[0] != n_turns:  
    print("\nInjected or circulating beam lost - adding penalty...\n")
    x1 = np.zeros(n_turns) 
    x1[0] = 100.   #set first bpm value to something high to simulate large first oscillation to give high penalty 
    x2 = np.zeros(n_turns)
    x2[0] = 100.
else:
    waveslice = np.array(df.iloc[0, :])  #projection of the waveforms, first row
    #Find the indices of correct rows corresponding to the injected and circulating beam 
    if bunch_index_width == 1:
        x1 = np.array(df.iloc[:, 0])
        x2 = np.array(df.iloc[:, 1])
    else:
        indices = waveslice.nonzero()[0]  #find indices of non-zero elements 
        ind_start = indices[0]
        ind1 = ind_start + bunch_index_width  #if waveform index width is know, pick the last one 
        ind2 = indices[np.where(indices > ind1)][0]   #second index simply the next value of "indices", larger than ind1
        #bpm position vectors for injected and circulating 
        x1 = np.array(df.iloc[:, ind1])
        x2 = np.array(df.iloc[:, ind2])

#Calculate reward 
x1bar = (abs(max(x1)) - abs(min(x1)))/2
x2bar = (abs(max(x2)) - abs(min(x2)))/2
reward = np.sqrt(x1bar**2 + x2bar**2)
print("\nReward is: {}\n".format(reward))

#Plot a heatmap to check out the data
#ax = sns.heatmap(df_org) 
#plt.show()

fig1 = plt.figure()
ax1 = fig1.add_subplot(1, 1, 1)
ax1.plot(x1)
ax1.plot(x2)
plt.show() 

"""

