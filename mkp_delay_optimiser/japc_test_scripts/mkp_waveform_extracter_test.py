#Small test script to extract the MKP waveforms via japc
import numpy as np
from pyjapc import PyJapc
import matplotlib.pyplot as plt

#Initialise JAPC
japc = PyJapc('SPS.USER.SFTPRO1', noSet=True)
#japc.setSelector("SPS.USER.SFTPRO1")

#Different MKP switches that we need
mkps = ['1A','1B','2A','2B','3A','3B','4A','4B','5A','5B','6A','6B','7A','7B','8A','8B']

#Iterate over the waveform data
kick_data = []
time_data = np.genfromtxt('data_and_sequences/sps_lhcpilot_time_data.txt')

for ele in mkps:
    mkp_test=japc.getParam("MKP.BA1.IPOC.TMR{}/Waveform#waveformData".format(ele))
    kick_data.append(mkp_test)

fig = plt.figure(figsize=(10, 7))
fig.suptitle('MKP kick waveforms - injected and circulating beam', fontsize=20)
ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
for i in range(16):
    plt.plot(time_data, kick_data[i], label='_nolegend_')
plt.ylabel('Kick [rad]')
plt.xlabel('Time [ns]')
#plt.xlim(4.5e3, 6e3)
plt.legend(facecolor='white', framealpha=1)
plt.show()