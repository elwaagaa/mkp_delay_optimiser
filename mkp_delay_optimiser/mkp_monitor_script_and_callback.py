"""
Script containing callback function to monitor the SPS injection: MKP waveforms, raw BPM data,
and injection oscillations
"""
import sys
sys.path.append('/afs/cern.ch/user/e/elwaagaa/cernbox/mkp-waveforms/cern-general-devices/cern_general_devices') 
from bpm import BPMLHC  #use if local changes have been made before mkp branch has been merged 
#from cern_general_devices.bpm import BPMLHC
from cern_general_devices.sps import SPS
import pyjapc
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from dataclasses import dataclass, field
from collections import deque


@dataclass
class DataCollector:
    buffer_length: int = 10
    bpm_data_buffer: deque = field(default_factory=deque) 
    bpm_data_injected = None
    bpm_data_circulating = None
    raw_bpm_pos = None
    mkp_kick_data = None 
    mkp_time_data= None 
    valid_data = False
    penalty: list = field(default_factory=list)

    def __post_init__(self):
        self.bpm_data_buffer = deque(maxlen=self.buffer_length)
        self.penalty = []

    def clear_all(self):
        self.bpm_data_buffer.clear()


japc = pyjapc.PyJapc("SPS.USER.LHC1", noSet=True)
acqStamp = 4615# 3415: #for LHCINDIV, 4615 for LHC1
bunch_index_width = 48  #number of bunches per train 
#japc = pyjapc.PyJapc("SPS.USER.LHC25NS", noSet=True)  #only for offline testing with different cycle 

data_collector = DataCollector()
sps = SPS()
bpm = BPMLHC(japc)

#Append MKP waveform data variables to aray 
mkps_IDs = [
    "1A",
    "1B",
    "2A",
    "2B",
    "3A",
    "3B",
    "4A",
    "4B",
    "5A",
    "5B",
    "6A",
    "6B",
    "7A",
    "7B",
    "8A",
    "8B",
]
kick_data_vars = []
for ele in mkps_IDs:
    kick_data_vars.append(
        "MKP.BA1.IPOC.TMR{}/Waveform#waveformData".format(ele)
    )


def callback(param, value, header):
    data_collector.valid_data = False
    print(value["acqC"])
    if not header["isFirstUpdate"] and value["acqC"] == acqStamp:
        print("good data")
        data_collector.bpm_pos_injected, data_collector.bpm_pos_circulating = bpm.extract_useful_bunches(bunches_per_batch=bunch_index_width)
        data_collector.raw_bpm_pos = bpm.extract_all_bunches()
        data_collector.mkp_kick_data = []
        for ele in kick_data_vars:
            mkp = japc.getParam(ele)
            data_collector.mkp_kick_data.append(mkp)
        data_collector.mkp_time_data = np.arange(0, 2*len(mkp), 2, dtype=int)  #equal time spacing for all waveforms 
        
        #Check that BPM arrays are not empty 
        if np.any(data_collector.bpm_pos_injected) and np.any(data_collector.bpm_pos_circulating):
            data_collector.valid_data = True


plt.ion()
fig, axis = plt.subplots(nrows=3, ncols=1, sharex=True, figsize=(8, 8)) #for injected and circulating bunches 
fig2, axis2 = plt.subplots(nrows=3, ncols=1, sharex=True, figsize=(8, 8))  #to generate heatmaps for all BPM data 
fig3, axis3 = plt.subplots(nrows=1, ncols=1, figsize=(6, 6))  #to plot the MKP waveform data 

japc.subscribeParam(sps.injection, callback, getHeader=True)
japc.startSubscriptions()

while True:
    if data_collector.valid_data:
        print("plotting")

        #iterate over the processed data to get the injection oscillations
        for i, ax in enumerate(axis.reshape(-1)):
            ax.clear()
            ax.plot(data_collector.bpm_pos_injected[i], label="Injected")
            ax.plot(data_collector.bpm_pos_circulating[i], label="Circulating")
            ax.set_ylabel("x [mm]")
            ax.text(0.88, 0.88, "BPM{}".format(i+1), weight='bold', transform=ax.transAxes,
                     bbox={'facecolor': 'green', 'alpha': 0.85})
            #ax.title.set_text("SPS BPM number {}".format(i+1))
            if i == 0:
                ax.legend(loc=2)
            if i == 2:
                ax.set_xlabel("#turns")
        plt.tight_layout()
        fig.canvas.draw()
        plt.pause(0.1)

        #iterate over the unprocessed data to observe the heatmap
        for i, ax in enumerate(axis2.reshape(-1)):
            ax.clear()
            s = sns.heatmap(data_collector.raw_bpm_pos[i], ax=ax, cbar=False)
            s.set(ylabel="#turns")
            #ax.title.set_text("SPS BPM number {}".format(i+1))
            ax.text(0.88, 0.88, "BPM{}".format(i+1), weight='bold', transform=ax.transAxes,
                     bbox={'facecolor': 'green', 'alpha': 0.85})
            if i == 2:
                s.set(xlabel="Bunch index number")
        plt.tight_layout()
        fig2.canvas.draw()
        plt.pause(0.1)

        #Plot the MKP waveforms 
        axis3.clear()
        for j in range(len(kick_data_vars)):
            axis3.plot(data_collector.mkp_time_data, data_collector.mkp_kick_data[j], label="Module {}".format(j+1))
        axis3.set_ylabel("Voltage [V]")
        axis3.set_xlabel("Time [ns]")
        axis3.legend()
        plt.tight_layout()
        plt.xlim(4000, 15000)
        plt.pause(0.1)
        fig3.canvas.draw()

        data_collector.valid_data = False

plt.show()
