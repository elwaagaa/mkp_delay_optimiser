# Small script to reset the current individual and general MKP delays as of April 5th 2022
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
import pickle
import datetime

# to access the parameter control
from pyjapc import PyJapc
from cern_general_devices import Device

# from cern_general_devices.mkp_delays import MKPdelays #---> THIS LINE WILL WORK WHEN THE GIT REPOSITORY WITH NEW MKP_DELAYS.PY HAS BEEN MERGED WITH THE MKP BRANCH
from cern_general_devices.mkp_delays import MKPdelays
from mkpdelays_env import MKPOptEnv

# Initiate JAPC session
japc = PyJapc("SPS.USER.LHCINDIV", noSet=False)

mkp_env = MKPOptEnv(japc)

TIMESTAMP = str(datetime.datetime.now()).replace(" ", "_").replace(":", "-")

all_vars = []
for i in range(8):
    all_vars.append("MKP.BA1.F3.PFN.{}/ExpertSettingDevice".format(i + 1))
all_vars.append("MKP.BA1.F3.CONTROLLER/Setting#arrKickDelay")


# RESET VALUES TO INITIAL VALUES
STANDARD_TIMESTAMP = "initial_mkp_delay_data/initial_MKP_delays_2022-04-05_09-30-14.424360.pickle"

with open("{}".format(STANDARD_TIMESTAMP), "rb") as handle:
    loaded_data = pickle.load(handle)
print("\nLoaded data is: {}\n".format(loaded_data))

# Create a dtau vector and send to the MKP reset
dtau_reset = np.zeros(9)
for i in range(8):
    dtau_reset[i] = loaded_data[i]["arrG2MainSwitchFineTimingDelay"][0]
dtau_reset[8] = loaded_data[8][1]  # general time shift

print(dtau_reset)
print(dtau_reset)

# Now set all MKP parameters
mkp_env.mkp_delays.mkp_delays.set_all_mkp_para(dtau_reset)
