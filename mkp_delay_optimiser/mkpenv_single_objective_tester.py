"""
Small script to test the compute_single_objective method of the mkpdelays_env.py with the PyBOBYQA optimiser
"""

import matplotlib.pyplot as plt
import matplotlib.ticker
import numpy as np
from scipy import optimize
import pybobyqa
import pickle
import datetime
from pyjapc import PyJapc


from mkpdelays_env import MKPOptEnv

# ------------- INITIALIZE JAPC, TRAINS PER BATCH AND PLOTSAVING ----------------------
japc = PyJapc(
    selector="SPS.USER.LHC1", incaAcceleratorName="SPS", noSet=True
)
acqStamp = 4615# 3415: #for LHCINDIV, 4615 for LHC1
bunch_index_width=48  #default value 1, if no bunch train 
saveplot = True  # choose to save plots or not
# -------------------------------------------------------------------

# ------- TEST MKP -----------
test_mkp_index = False  # If False, all MKPs and general shift are included
mkp_index_test = [
    8
]  # if first and second individual shift + general shift are desired
# ---------------------------

if test_mkp_index:
    mkp_env = MKPOptEnv(japc=japc, mkp_index=mkp_index_test, bunch_index_width=bunch_index_width, acqStamp=acqStamp)
else:
    mkp_env = MKPOptEnv(japc, bunch_index_width=bunch_index_width, acqStamp=acqStamp)

print("\nx0 action is: {}\n".format(mkp_env.x0_action))
test_action = mkp_env.x0_action_norm
print("Normalised x0 action is: {}\n".format(test_action))
isokay = np.all(test_action >= -1) and np.all(test_action <= 1)
print("x0 action within limits: {}".format(isokay))

mkp_env.reset()

dtau0 = mkp_env.get_initial_params()
print(
    "\n\nInitial conditions from fist taken normalised action: tau0 = {}\n\n".format(
        dtau0
    )
)

# Possibility to test one call of the compute single objective method
# reward0 = mkp_env.compute_single_objective(dtau0)

iterations = 1
positions = np.zeros(iterations)
iter_needed = np.zeros(iterations)

data_error = {}
total_data = []


TIMESTAMP = str(datetime.datetime.now()).replace(" ", "_").replace(":", "-")
# ROOT_SAVE = "/afs/cern.ch/user/e/elwaagaa/cernbox/mkp-waveforms/mkp_optimisation_real_sps_test/mkp-waveform-optimiser/mkp-waveform-optimiser/plots_and_data/"
print("Timestamp is {}".format(TIMESTAMP))

fig, axis = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(11, 8))
axis[0].plot([], [])
axis[1].plot([], [])
axis[0].yaxis.label.set_size(16)
axis[0].tick_params(axis="y", labelsize=13)
axis[1].yaxis.label.set_size(16)
axis[1].xaxis.label.set_size(16)
axis[1].tick_params(axis="x", labelsize=13)
axis[1].tick_params(axis="y", labelsize=13)
legend_names = [
    "Switch 1",
    "Switch 2",
    "Switch 3",
    "Switch 4",
    "Switch 5",
    "Switch 6",
    "Switch 7",
    "Switch 8",
    "General shift",
]
#only select the relevant labels if we optimised for given MKP indices 
if test_mkp_index:
    legend_names = [legend_names[k] for k in mkp_index_test]
actions_all = []
penalty = []

# Define function to minimise, which also generates plots and saves data
def minimizing_objective(dtau):

    actions = np.array(dtau)
    reward = mkp_env.compute_single_objective(actions)
    # reward = 10.0      #test reward

    print("\nREWARD IS {}\n".format(reward))
    penalty.append(reward)
    actions_all.append(actions)

    # Serialize and save data
    if saveplot:
        with open(
            "plots_and_data/actors_mkp_delays_log_data_{}.pickle".format(
                TIMESTAMP
            ),
            "wb",
        ) as handle:
            pickle.dump(
                mkp_env.log_data, handle, protocol=pickle.HIGHEST_PROTOCOL
            )

    for ax in axis:
        ax.cla()
    axis[0].plot(penalty, color='r', marker=10, ms=12)
    axis[0].set(ylabel="Sq sum BPM positions [mm]")
    axis[0].set_yscale("log")
    # axis[0].set_yticks([5.0, 8.0])  #add extra ticks of BPM positions
    # axis[0].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    axis[1].plot(actions_all, label=legend_names, marker=11, ms=12)
    axis[1].set(xlabel="#Iterations", ylabel="Actions")
    axis[1].legend(loc="right", fancybox=True, bbox_to_anchor=(1.125, 0.6))
    plt.subplots_adjust(wspace=0, hspace=0)
    fig.canvas.draw()
    axis[0].get_shared_x_axes().join(
        axis[0], axis[1]
    )  # make the subplots share the x-axis of iterations
    if saveplot:
        fig.savefig(
            "plots_and_data/mkp_delays_actors_plot_{}.png".format(TIMESTAMP),
            dpi=250,
        )
    plt.pause(0.001)

    return reward


# solution for 225 ns
# solution_xmin = [
#     -0.06695098,
#     -0.08020004,
#     -0.09076124,
#     -0.41813021,
#     -0.41373875,
#     -0.14637182,
#     0.27850253,
#     0.49669337,
#     -0.14965273,
# ]

# solution for 200 ns
solution_xmin = [
    0.0,
    0.26,
    -0.07,
    0.027,
    0.34,
    0.26,
    0.57,
    0.9,
    -0.27,
]
# solution_xmin = [
#     0.04033597,
#     0.00737133,
#     0.01249344,
#     0.01045089,
#     0.01045089,
#     -0.09744823,
#     0.51151124,
#     0.61860183,
#     0.01823761,
# ]


#minimizing_objective(solution_xmin)
#plt.show()

# PyBOBYQA optimisation
if test_mkp_index:
    lim_length = len(mkp_index_test)
else:
    lim_length = 9 #all 8 individual shifts + general shift 

upper_lim = np.ones(lim_length)
lower_lim = -1 * np.ones(lim_length)
bounds = (lower_lim, upper_lim)


soln = pybobyqa.solve(
    minimizing_objective,
    dtau0,
    bounds=bounds,
    rhobeg=0.5,
    rhoend=0.02,
    objfun_has_noise=True,
    # seek_global_minimum=True,
    print_progress=True,
)
print(soln)

print("Setting solution to machine")
result = soln.x
minimizing_objective(result)

print("\nReward: {}\n".format(penalty))
print("Action: {}\n".format(actions_all))

plt.show()


# Possibility to check pickled data by unserializing it
"""
with open("plots_and_data/actors_mkp_delays_log_data_{}.pickle".format(TIMESTAMP), "rb") as handle:
    data = pickle.load(handle)
print("\nData is: {}".format(data))
"""


# -------------------- Scipy optimisation minimization ---------------------------------
"""
#upper_lim = np.ones(9)
#lower_lim = -1*np.ones(9)
#bounds = tuple(zip(lower_lim, upper_lim))
#results = optimize.minimize(minimizing_objective, dtau0, bounds=bounds, method='Powell', options={'disp': True, 'maxiter' : 1})

print("\n\nResult:")
print(results)

print("\nTesting compute single objective with {}:".format(results.x))
final_loss = mkp_env.compute_single_objective(results.x)
#print("Penalty:\n{}".format(penalty))

plt.figure()
plt.plot(-1 * 1000 * np.array(mkp_env.log_data["out"]))
plt.xlabel("Iterations")
plt.ylabel("BPM positions [mm]")

plt.show()
"""
