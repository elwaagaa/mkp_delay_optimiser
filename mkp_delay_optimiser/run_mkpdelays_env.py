# TEST SCRIPT TO RUN THE MKPDELAYS ENVIRONMENT, BOTH WITH REAL AND SIMULATED (FAKE) DATA
#Check normalisation and limits 

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import sys

from pyjapc import PyJapc
from mkpdelays_env import MKPOptEnv
from real_mkp_delays import RealMKPdelays

japc = PyJapc("SPS.USER.LHCINDIV", noSet=True)
# Test environment


# test different limits
mkp_env = MKPOptEnv(japc)


x0_norm = mkp_env.x0_action_norm
print("Normalized action: {}".format(x0_norm))

#print("\nAll MKP delay variables: {}\n".format(mkp_env.mkp_delays.mkp_delays.all_vars))
#data_test = japc.getParam(mkp_env.mkp_delays.mkp_delays.all_vars)
#print("Data test for all MKP variables: {}\n".format(data_test))

mkp_env.reset()
# stop here to test set of just obtained parameters


x0 = mkp_env.compute_single_objective(x0_norm)


print("\nSingle objective function computation test: {}\n".format(x0_norm))

print("\n\nCheck normalisation both ways and that these match:\n")
print("Unnormalised normalised action: {} \nNormal action: {}\n\n".format(mkp_env.inv_norm_data(x0_norm), mkp_env.x0_action))


test_action_limits = np.ones_like(x0_norm)

result_limit = mkp_env.compute_single_objective(test_action_limits)
print("\nTest action limits, we intentionally go out of bounds for the action.\nThe clipped action should match exactly the given parameter limits:")
print(mkp_env.inv_norm_data(test_action_limits))
print(mkp_env.inv_norm_data(-test_action_limits))

#Also make sure that limits from top environment have reached the device clas
print("\nLow-level MKP device class limits from top: \n{}".format(mkp_env.mkp_delays.mkp_delays.limits))

#Print log data of environment 
print("\nLog data BPM positions: \n{}".format(mkp_env.log_data['bpm_pos']))
